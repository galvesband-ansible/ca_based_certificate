Role Name
=========

Buils a certificate suitable for web server usage signed by a 
certificate authority created by the 
[certificate_authority](https://gitlab.com/galvesband-ansible/certificate_authority) 
role.

The `certificate_authority` role builds a very simple CA on the
server it is applied to. This role builds a new SSL certificate
and signs it with that remote certificate authority.

Beware, this role and `certificate_authority` are not meant to be
used in public or production environments. Both of them where
made as an experiment, as a learn experience. At most, they can
be used in a small local network.

Requirements
------------

None.

Role Variables
--------------

This role needs access to the server that hosts the certificate 
authority and asummes that remote host is accesible throught
ansible and the CA structure is the one created by the
`certificate_authority` role.

 - `ca_based_certificate_fqdn`: FQDN of the new certificate. For
   example, `my-subdomain.mylocaldomain.local`.

 - `ca_based_certificate_ca_name`: Name of the CA created with
   the `certificate_authority` role.

 - `ca_based_certificate_ca_host`: Host in the ansible inventory
   that created (and hosts) the certificate authority.

Dependencies
------------

- `certificate_authority`. Although this role doesn't use it directly,
  it relays on a filesystem structure builded by the 
  `certificate_authority` role to host de certificate authority.

Example Playbook
----------------

Simple example of two servers, one holding the CA and other
were a derived certificate will be used.

```yml
- name: The CA server
  hosts: ca-host-machine
  gather_facts: yes
  become: yes
  roles:

   - name: certificate_authority
     vars: 
       ca_name: my-personal-ca

- name: The other server
  hosts: final-server
  gather_facts: yes
  become: yes
  roles:

   - name: ca_based_certificate
     vars: 
       ca_based_certificate_fqdn: my-personal-domain.local
       ca_based_certificate_ca_name: my-personal-ca
       ca_based_certificate_ca_host: ca-host-machine
     
```

License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).
